public class SUV {
    private int id;
    private int doors;
    private boolean windshiled;
    private int wheels;
    private boolean isLeather;
    private int seats;
    private String suspensionType;
    private boolean hasRemovableRoof;

    public SUV(int id, int doors, boolean windshiled, int wheels, boolean isLeather, int seats, String suspensionType, boolean hasRemovableRoof) {
        this.id = id;
        this.doors = doors;
        this.windshiled = windshiled;
        this.wheels = wheels;
        this.isLeather = isLeather;
        this.seats = seats;
        this.suspensionType = suspensionType;
        this.hasRemovableRoof = hasRemovableRoof;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDoors() {
        return doors;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }

    public boolean isWindshiled() {
        return windshiled;
    }

    public void setWindshiled(boolean windshiled) {
        this.windshiled = windshiled;
    }

    public int getWheels() {
        return wheels;
    }

    public void setWheels(int wheels) {
        this.wheels = wheels;
    }

    public boolean isLeather() {
        return isLeather;
    }

    public void setLeather(boolean leather) {
        isLeather = leather;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public String getSuspensionType() {
        return suspensionType;
    }

    public void setSuspensionType(String suspensionType) {
        this.suspensionType = suspensionType;
    }

    public boolean isHasRemovableRoof() {
        return hasRemovableRoof;
    }

    public void setHasRemovableRoof(boolean hasRemovableRoof) {
        this.hasRemovableRoof = hasRemovableRoof;
    }

    @Override

    public String toString() {
        return "SUV : " +
                "id=" + id +
                ", doors=" + doors +
                ", windshiled=" + windshiled +
                ", wheels=" + wheels +
                ", isLeather=" + isLeather +
                ", seats=" + seats +
                ", suspensionType= " + suspensionType +
                ", hasRemovableRoof= " + hasRemovableRoof+
                ".";
    }
}
