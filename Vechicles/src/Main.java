import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        ArrayList<Car> cars = new ArrayList<>();
        ArrayList<SUV> suvs = new ArrayList<>();
        cars.add(new Car(cars.size(), 4, true, 4, false, 5));
        cars.add(new Car(cars.size(), 3, true, 4, true, 2));
        cars.add(new Car(cars.size(), 4, false, 4, true, 4));
        suvs.add(new SUV(suvs.size(), 4, false, 4, true, 4, "asd", true));
        suvs.add(new SUV(suvs.size(), 4, false, 4, true, 4, "asd2", false));

        showMenu();

        int userOption = getUserInput(scanner);
        if(userOption==1){
                System.out.println("You choose - View Cars -");
                showHeadline("Car List");
                for (int i = 0; i < cars.size(); i++) {
                    System.out.println(cars.get(i));
                }
                System.out.println();
                System.out.println("------------------");
                System.out.println();

            }else if(userOption == 2){
                System.out.println("You choose - View SUVs -");
                showHeadline("Suv List");
                for (int i = 0; i < suvs.size(); i++) {
                    System.out.println(suvs.get(i));
                }
                System.out.println();
                System.out.println("------------------");
                System.out.println();


            }else if(userOption == 3){

                System.out.println("You choose - Add -");
                System.out.println("1. Add new car");
                System.out.println("2. Add new suv");
                int s = getUserInput(scanner);
                if(s == 1){

                    showHeadline("You choose add new car");

                    System.out.print("Number of doors : ");
                    int doors = getUserInput(scanner);

                    System.out.print("Does it have windshield ");
                    boolean hasW = scanner.nextBoolean();

                    System.out.print("Number of wheels: ");
                    int wheels = getUserInput(scanner);

                    System.out.print("Is it leather ? ");
                    boolean hasL = scanner.nextBoolean();
                    System.out.print("Number of seats: ");
                    int seats = getUserInput(scanner);
                    System.out.println();

                    Car car = new Car(cars.size(), doors, hasW, wheels, hasL, seats);
                    cars.add(car);

                }else if(s ==2 ){
                    // TODO: Add new Suv in the list
                    showHeadline("You Choose add new Suv");

                    System.out.print("Number of doors : ");
                    int doors = getUserInput(scanner);

                    System.out.print("Does it have windshield ");
                    boolean hasW = scanner.nextBoolean();

                    System.out.print("Number of wheels: ");
                    int wheels = getUserInput(scanner);

                    System.out.print("Is it leather ? ");
                    boolean hasL = scanner.nextBoolean();

                    System.out.print("Number of seats: ");
                    int seats = getUserInput(scanner);

                    System.out.println("What is the suspensionType? ");
                    String suspensionType = getStringInput();

                    System.out.println("Is it hasRemovableRoof? ");
                    boolean hasRR = scanner.nextBoolean();

                    SUV suv = new SUV(suvs.size(), doors, hasW, wheels, hasL, seats, suspensionType, hasRR);
                    suvs.add(suv);

                }else{
                    System.err.println("Wrong option");
                }

            }else if(userOption == 4){

                System.out.println("You choose - Remove -");
                System.out.println("1. Remove car");
                System.out.println("2. Remove suv");

                int removeBy = getUserInput(scanner);
                if (removeBy == 1){
                    showHeadline("Remove car by id");
                    showHeadline("Provide the id of the car to be removed");
                    int carIdToRemove = getUserInput(scanner);

                    boolean wasDeleted = false;
                    for (int i = 0; i < cars.size(); i++) {
                        Car car = cars.get(i);
                        if(car.getId() == carIdToRemove){
                            cars.remove(car);
                            wasDeleted = true;
                        }
                    }
                    if(wasDeleted){
                        showHeadline("Car with id" + carIdToRemove + " was removed from the car list");
                    }else {
                        showHeadline("Car with id" + carIdToRemove + " was not found in the database ");
                    }
                }
                // TODO: Remove Suv from the 'database'
                else if (removeBy == 2){
                    showHeadline("Remove SUV by id");
                    showHeadline("Provide the id of the SUV");

                    int suvIdToRemove = getUserInput(scanner);

                    boolean wasDeleted = false;
                    for (int i = 0; i < suvs.size(); i++) {
                        SUV suv = suvs.get(i);
                        if(suv.getId() == suvIdToRemove){
                            suvs.remove(suv);
                            wasDeleted=true;
                        }
                        if(wasDeleted){
                            showHeadline("SUV with id " + suvIdToRemove + " was removed from the SUV list");
                        }else {
                            showHeadline("SUV with id " + suvIdToRemove + " was not found in the database ");
                        }

                    }
                }


            }else if(userOption == 5){
                System.out.println("1. Update car ");
                System.out.println("2. Update SUV ");
                int subNumber= getUserInput(scanner);

                if(subNumber==1){
                    System.out.println("You chose to update car ");
                    System.out.println("Provide the id of the car ");
                    int idToUpdate = getUserInput(scanner);
                    Car carToUpdate = null;
                    for (int i = 0; i < cars.size(); i++) {
                        Car car = cars.get(i);
                        if (car.getId()==idToUpdate){
                            carToUpdate=car;
                        }
                    }
                    if(carToUpdate!=null){
                        System.out.println("Car was found in the database.");
                        System.out.println("Your car is: "+ carToUpdate);

                        System.out.println("Provide new door number: ");
                        int newdoors=getUserInput(scanner);
                        System.out.println("Provide if has windshield? ");
                        boolean newwindshiled=scanner.nextBoolean();
                        System.out.println("Provide new wheels number: ");
                        int newwheels=getUserInput(scanner);
                        System.out.println("Provide if isLeather?");
                        boolean isLeather=scanner.nextBoolean();
                        System.out.println("Provide new seats number: ");
                        int newseats=getUserInput(scanner);

                    carToUpdate.setDoors(newdoors);
                    carToUpdate.setWindshiled(newwindshiled);
                    carToUpdate.setWheels(newwheels);
                    carToUpdate.setLeather(isLeather);
                    carToUpdate.setSeats(newseats);


                    }
                    else {
                        System.out.println("Car was not found in the database.");
                    }



                }
                else if (subNumber==2){
                    System.out.println("You chose to update SUV ");
                    System.out.println("Provide the id of the SUV ");

                    int subNumber2=getUserInput(scanner);
                SUV  suvToUpdate=null;
                    for (int i = 0; i <suvs.size() ; i++) {
                        if(suvs.get(i).getId()==subNumber2)
                            suvToUpdate=suvs.get(i);
                    }
                    if(suvToUpdate!=null){
                        System.out.println("The SUV was found in the database: ");
                        System.out.println("Your SUV: "+ suvToUpdate);

                        System.out.println("Provide new door number: ");
                        int newdoor=getUserInput(scanner);
                        System.out.println("Provide if it has windshield: ");
                        boolean windshield=scanner.nextBoolean();
                        System.out.println("Provide new wheels number: ");
                        int newwheels=getUserInput(scanner);
                        System.out.println("Provide if it is leather? ");
                        boolean isLeather=scanner.nextBoolean();
                        System.out.println("Provide new number of seats: ");
                        int newseats=getUserInput(scanner);
                        scanner.nextLine();
                        System.out.println("Provide suspensiontype: ");
                        String suspensionType=scanner.nextLine();
                        System.out.println("Provide if it has RemovableRoof? ");
                        boolean hasRemovableRoof=scanner.nextBoolean();

                        suvToUpdate.setDoors(newdoor);
                        suvToUpdate.setWindshiled(windshield);
                        suvToUpdate.setWheels(newwheels);
                        suvToUpdate.setLeather(isLeather);
                        suvToUpdate.setSeats(newseats);
                        suvToUpdate.setSuspensionType(suspensionType);
                        suvToUpdate.setHasRemovableRoof(hasRemovableRoof);

                    }
                    else
                        System.out.println("Your SUV was not found in the database.");


                }



                // TODO: Ask user what to update ( Car or Suv )
                // TODO: Display a menu
                // TODO: After entering what to update, ask for the id of the car / suv to be updated
                // TODO: Display the car/suv to be modifie
                // TODO: Ask the user with all information to modify
                // TODO: If the user does not want to change a property, the same property must be entered

            }
            showMenu();
            userOption = getUserInput(scanner);
        }








    private static int getUserInput(Scanner scanner) {
        return scanner.nextInt();
    }

    public static void showHeadline(String element){
        System.out.println(" --- " + element + " ---");
    }

    public static void showMenu(){
        System.out.println(" --- User Menu ---");
        System.out.println("1. View Cars");
        System.out.println("2. View SUV");
        System.out.println("3. Add");
        System.out.println("4. Remove");
        System.out.println("5. Update");
        System.out.println("6. Exit");
        System.out.println("------------");

    }

    public static String getStringInput(){
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
