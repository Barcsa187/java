public class Car {

    private int id;
    private int doors;
    private boolean windshiled;
    private int wheels;
    private boolean isLeather;
    private int seats;

    public Car(int id, int doors, boolean windshiled, int wheels, boolean isLeather, int seats){
        this.id=id;
        this.doors=doors;
        this.windshiled=windshiled;
        this.wheels=wheels;
        this.isLeather=isLeather;
        this.seats=seats;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDoors() {
        return doors;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }

    public boolean isWindshiled() {
        return windshiled;
    }

    public void setWindshiled(boolean windshiled) {
        this.windshiled = windshiled;
    }

    public int getWheels() {
        return wheels;
    }

    public void setWheels(int wheels) {
        this.wheels = wheels;
    }

    public boolean isLeather() {
        return isLeather;
    }

    public void setLeather(boolean leather) {
        isLeather = leather;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    @Override
    public String toString() {
        return "Car : " +
                "id=" + id +
                ", doors=" + doors +
                ", windshiled=" + windshiled +
                ", wheels=" + wheels +
                ", isLeather=" + isLeather +
                ", seats=" + seats +
                '.';
    }
}
