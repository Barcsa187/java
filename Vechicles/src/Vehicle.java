public class Vehicle {

    private int id;
    private int doors;
    private boolean windshiled;
    private int wheels;

    public Vehicle(int id, int doors, boolean windshiled, int w){
        this.id=id;
        this.doors=doors;
        this.windshiled=windshiled;
        wheels=w;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDoors() {
        return doors;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }

    public boolean isWindshiled() {
        return windshiled;
    }

    public void setWindshiled(boolean windshiled) {
        this.windshiled = windshiled;
    }

    public int getWheels() {
        return wheels;
    }

    public void setWheels(int wheels) {
        this.wheels = wheels;
    }
}
