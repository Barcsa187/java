package com.company;

public class InstrumentMuzical {
    protected int id;
    protected String culoare;
    protected String brand;
    protected String model;
    protected int pret;

    public InstrumentMuzical(int id, String culoare, String brand, String model, int pret) {
        this.id = id;
        this.culoare = culoare;
        this.brand = brand;
        this.model = model;
        this.pret = pret;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCuloare() {
        return culoare;
    }

    public void setCuloare(String culoare) {
        this.culoare = culoare;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getPret() {
        return pret;
    }

    public void setPret(int pret) {
        this.pret = pret;
    }
}
