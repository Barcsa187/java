public class Aenimal {


    private String specie;
    private int nrPicioare;
    private boolean mort;

    public Aenimal(String specie){
        this.specie= specie;
    }
    public Aenimal(int nrPicioare, boolean mort){
        this.nrPicioare=nrPicioare;
        this.mort=mort;
    }
    public Aenimal(String specie,int nrPicioare,boolean mort){
        this.specie= specie;
        this.nrPicioare=nrPicioare;
        this.mort=mort;
    }

    public void afiseazaAenimal(){
        System.out.println("Specia animalului este " + specie);
        System.out.println("Are " + nrPicioare + " picioare");
        if(mort==true)
        System.out.println(" este mort");
        else
            System.out.println(" nu este mort");
    }
}
class Main{
    public static void main(String[] args) {
        Aenimal caine = new Aenimal("husky",4,true);
        caine.afiseazaAenimal();
        Aenimal pisica = new Aenimal(3,false);
        pisica.afiseazaAenimal();

    }
}